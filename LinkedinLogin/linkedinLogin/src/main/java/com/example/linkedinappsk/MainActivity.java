package com.example.linkedinappsk;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

//import android.R;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.linkedinappsk.DeepLinkActivity;
import com.linkedin.platform.DeepLinkHelper;
import com.linkedin.platform.LISession;
import com.linkedin.platform.LISessionManager;
import com.linkedin.platform.errors.LIAuthError;
import com.linkedin.platform.errors.LIDeepLinkError;
import com.linkedin.platform.listeners.AuthListener;
import com.linkedin.platform.listeners.DeepLinkListener;
import com.linkedin.platform.utils.Scope;

public class MainActivity extends Activity {

	Context context;
	Button logout;

	boolean successStatus = false;

	public static final String PACKAGE_MOBILE_SDK_SAMPLE_APP = "com.example.linkedinappsk";

	protected static final String TAG = "ACTIVE";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_ativity);
		context = this;

		printKeyHash();

		// final Activity thisActivity = this;
		setUpdateState();

		// Initialize session
		Button liLoginButton = (Button) findViewById(R.id.loginbtn);

		liLoginButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				LISessionManager.getInstance(getApplicationContext()).init(
						(Activity) context, buildScope(), new AuthListener() {

							@Override
							public void onAuthSuccess() {

								successStatus = true;

								setUpdateState();
								Toast.makeText(getApplicationContext(),
										"Successfully Signed In !!",
										Toast.LENGTH_LONG).show();

							}

							@Override
							public void onAuthError(LIAuthError error) {
								setUpdateState();
								// ((TextView) findViewById(R.id.View1))
								// .setText(error.toString());
								Toast.makeText(getApplicationContext(),
										"Sign in failed ", Toast.LENGTH_LONG)
										.show();
							}
						}, true);
				Button login = (Button) view;
				login.setVisibility(View.GONE);
				View v = findViewById(R.id.logoutbtn);
				v.setVisibility(View.VISIBLE);
				View m = findViewById(R.id.myProfile);
				m.setVisibility(View.VISIBLE);
			}
		});

		// Clear session
		Button liForgetButton = (Button) findViewById(R.id.logoutbtn);
		View b = findViewById(R.id.logoutbtn);
		b.setVisibility(View.GONE);

		liForgetButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				setClickable(true);
				LISessionManager.getInstance(getApplicationContext())
						.clearSession();
				setUpdateState();
				Toast.makeText(getApplicationContext(),
						"Successfully Signed Out ", Toast.LENGTH_LONG).show();

				View v1 = findViewById(R.id.logoutbtn);
				v1.setVisibility(View.GONE);
				View m = findViewById(R.id.myProfile);
				m.setVisibility(View.GONE);
				View i = findViewById(R.id.loginbtn);
				i.setVisibility(View.VISIBLE);
			}

		});

		// Go to API calls page
		/*
		 * Button liApiCallButton = (Button) findViewById(R.id.apiCall);
		 * liApiCallButton.setOnClickListener(new View.OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { Intent intent = new
		 * Intent(MainActivity.this, ApiActivity.class); startActivity(intent);
		 * } });
		 */
		// Go to DeepLink page

		Button myButton = (Button) findViewById(R.id.myProfile);
		View m = findViewById(R.id.myProfile);
		m.setVisibility(View.GONE);
		myButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Button liMyProfileButton = (Button) findViewById(R.id.myProfile);
				liMyProfileButton
						.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								DeepLinkHelper deepLinkHelper = DeepLinkHelper
										.getInstance();
								deepLinkHelper.openCurrentProfile(
										MainActivity.this,
										new DeepLinkListener() {
											@Override
											public void onDeepLinkSuccess() {
												((TextView) findViewById(R.id.deeplink_error))
														.setText("");
											}

											@Override
											public void onDeepLinkError(
													LIDeepLinkError error) {
												((TextView) findViewById(R.id.deeplink_error))
														.setText(error
																.toString());
											}
										});
							}
						});
			}
		});

	}

	/*
	 * @Override public void onClick(View v) { Intent intent = new
	 * Intent(MainActivity.this, DeepLinkActivity.class); startActivity(intent);
	 * } }); }
	 */

	private void setClickable(boolean b) {
		// TODO Auto-generated method stub

	}

	// Compute application package and hash
	public void onClick(View v) {
		try {
			PackageInfo info = getPackageManager().getPackageInfo(
					PACKAGE_MOBILE_SDK_SAMPLE_APP,
					PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());

				((TextView) findViewById(R.id.pckText))
						.setText(info.packageName);

				String response = Base64.encodeToString(md.digest(),
						Base64.NO_WRAP);

				String stat = "";

				if (successStatus) {
					stat = "Login Successfull !";
				} else {
					stat = "Login Failed";
				}
				((TextView) findViewById(R.id.pckHashText)).setText(stat);
			}
		} catch (PackageManager.NameNotFoundException e) {
			Log.d(TAG, e.getMessage(), e);
		} catch (NoSuchAlgorithmException e) {
			Log.d(TAG, e.getMessage(), e);
		}
	}

	private void printKeyHash() {
		// Add code to print out the key hash
		try {
			PackageInfo info = getPackageManager().getPackageInfo(
					getPackageName(), PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				Log.e("KeyHash:", "dfeEfA7uQPKeFYmG70mEohDQlOo=");
				Log.e("KeyHash:",
						Base64.encodeToString(md.digest(), Base64.DEFAULT));
			}
		} catch (NameNotFoundException e) {
			Log.e("KeyHash:", e.toString());
		} catch (NoSuchAlgorithmException e) {
			Log.e("KeyHash:", e.toString());
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		LISessionManager.getInstance(getApplicationContext()).onActivityResult(
				this, requestCode, resultCode, data);
	}

	private void setUpdateState() {
		LISessionManager sessionManager = LISessionManager
				.getInstance(getApplicationContext());
		LISession session = sessionManager.getSession();
		boolean accessTokenValid = session.isValid();

		// ((TextView) findViewById(R.id.View1)).setText(accessTokenValid ?
		// session
		// .getAccessToken().toString()
		// : "Sync with LinkedIn to enable these buttons");
		// ((Button) findViewById(R.id.apiCall)).setEnabled(accessTokenValid);
		// ((Button) findViewById(R.id.deeplink)).setEnabled(accessTokenValid);
	}

	private static Scope buildScope() {
		return Scope.build(Scope.R_BASICPROFILE, Scope.W_SHARE);
	}

}
